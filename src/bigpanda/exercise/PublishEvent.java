package bigpanda.exercise;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class PublishEvent implements Runnable{

	private String eventData;
	public PublishEvent(String pEventData) {
		eventData = pEventData;
	}
	
	private EventModel validateAndConvertToObject()
	{
		Gson gsno = new Gson();
		EventModel eventModel = null;
		try
		{
			eventModel = gsno.fromJson(eventData, EventModel.class);
		}
		catch(JsonSyntaxException e)
		{
			//not valid data
		}
		return eventModel;
	}
	
	@Override
	public void run() {
		try
		{
			EventModel eventModel = validateAndConvertToObject();
			if(eventModel != null)
				Broker.instance.putEvent(eventModel);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
