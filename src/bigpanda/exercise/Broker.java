package bigpanda.exercise;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class Broker {
	private final int MAX_QUEUE = 100;
	private Queue<EventModel> _queue = new ArrayBlockingQueue<EventModel>(MAX_QUEUE);
	public static Broker instance = new Broker();
	private Object lockObject = new Object();
	private Broker()
	{
		
	}
	
	public void putEvent(EventModel pEventModel) throws InterruptedException
	{
		synchronized (lockObject) {
			while(_queue.size() == MAX_QUEUE)
			{
				lockObject.wait();			
			}
			_queue.add(pEventModel);
			lockObject.notify();
		}
	}
	
	public EventModel getEvent() throws InterruptedException
	{
		synchronized (lockObject) {
			while(_queue.size() == 0)
			{
				lockObject.wait();
			}
			lockObject.notify();
			return _queue.poll();
		}
	}
	
}
