package bigpanda.exercise;

public class StartStream {

	public static void main(String[] args) throws InterruptedException {
			String filePath = args[0];
			StreamReader streamReader = new StreamReader(filePath);
			Thread streamThread = new Thread(streamReader);
			streamThread.start();	
			
			ConsumeEvents consume = new ConsumeEvents();
			Thread startConsume = new Thread(consume);
			startConsume.start();
			
			SimpleHTTPServer httpServer = new SimpleHTTPServer();
			Thread startHTTPServer = new Thread(httpServer);
			startHTTPServer.start();
	}
	
}
