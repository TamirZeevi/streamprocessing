package bigpanda.exercise;

public class ConsumeEvents implements Runnable{

	@Override
	public void run() {
		while(true)
		{
			try {
				EventModel eventModel = Broker.instance.getEvent();
				if(eventModel != null)
					StatsEvents.instance.addEvent(eventModel);
				Thread.sleep(10);
				} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
