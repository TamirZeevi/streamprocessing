package bigpanda.exercise;

import java.util.HashMap;

public class StatsEvents {

	private int WordsCount = 0;
	private HashMap<String, Integer> _mapByEventType = new HashMap<String,Integer>();
	public static StatsEvents instance = new StatsEvents();
	private StatsEvents() {}
	
	public synchronized void addEvent(EventModel pEventModel)
	{
		int currentIndex = 0;
		if(_mapByEventType.containsKey(pEventModel.event_type))
		{
			currentIndex = _mapByEventType.get(pEventModel.event_type);
		}
		
		_mapByEventType.put(pEventModel.event_type, ++currentIndex);
		WordsCount += pEventModel.data.split(" ").length;
	}
	
	public synchronized String getStats()
	{
		StringBuffer sb = new StringBuffer();
		for (String eventType : _mapByEventType.keySet())
		{
			sb.append("EventType: " + eventType + " Count: " + _mapByEventType.get(eventType)+ "\r\n");
		}
		sb.append("Total Words Count: " + WordsCount);
		
		return sb.toString();
	}
	
}
