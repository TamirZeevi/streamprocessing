package bigpanda.exercise;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;

public class SimpleHTTPServer implements Runnable{

	private ServerSocket server;

	@Override
	public void run() {
		try
		{
			server = new ServerSocket(8888); 
			while (true) 
			{ 
				Socket socket = server.accept(); 
				HandleRquest handleRequest = new HandleRquest(socket);
				Thread socketThread = new Thread(handleRequest);
				socketThread.start();
			}
			
		}
		catch(Exception e)
		{
			
		}
	
	}
	
	class HandleRquest implements Runnable
	{
		private Socket _socket;
		public HandleRquest(Socket pSocket)
		{
			_socket = pSocket;
		}
		
		@Override
		public void run() {
			StringBuilder httpResponse = new StringBuilder("HTTP/1.1 200 OK\r\n\r\n");
			httpResponse.append(StatsEvents.instance.getStats());
			try {
				_socket.getOutputStream().write(httpResponse.toString().getBytes("UTF-8"));
				_socket.close();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	

}
