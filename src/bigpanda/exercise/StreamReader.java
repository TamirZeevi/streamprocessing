package bigpanda.exercise;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class StreamReader implements Runnable{
	
	private String fileName;
	public StreamReader(String pFileName)
	{
		fileName = pFileName;
	}
	
	private void readStream()
	{
		try
		{
			Process pr = Runtime.getRuntime().exec(fileName);	
		    BufferedReader in = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		    String line;
		    while ((line = in.readLine()) != null) {
		        System.out.println(line);
		        PublishEvent publishEvent = new PublishEvent(line);
		        Thread publishThread = new Thread(publishEvent);
		        publishThread.start();
		    }
		    pr.waitFor();
		    in.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	@Override
	public void run() {
		readStream();
	}

}
